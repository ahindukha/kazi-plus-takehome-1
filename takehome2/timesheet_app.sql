CREATE DATABASE timesheet_app;
CREATE TABLE employee( 
   employee_id INT, 
   first_name VARCHAR(20), 
   last_name VARCHAR(20), 
   phone INT,
   email VARCHAR(30) ,
   PRIMARY KEY(employee_id ) 
);
CREATE TABLE project( 
   project_id INT, 
   name VARCHAR(30), 
   start_date DATE, 
   end_date DATE,
   PRIMARY KEY(project_id) 
);
CREATE TABLE task( 
   task_id INT, 
   employee_id INT references employee(employee_id), 
   project_id INT references project(project_id), 
   date_done INT, 
   task VARCHAR(50),
   time_taken INT,
   comments VARCHAR(255),
   PRIMARY KEY(task_id) 
);
