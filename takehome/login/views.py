from django.shortcuts import render,redirect
from .models import UserData
#from .forms import UserForm

def home(request):
    userdata = UserData.objects.all
    context = {'userdata':userdata}
    return render(request,'home.html',context)



def addinfo(request):
    projectname = request.POST['projectname']
    task = request.POST['task']
    comment = request.POST['comment']
    datedone = request.POST['datedone']

    userdata = UserData(projectname= projectname,task = task,comment = comment,datedone = datedone)
    userdata.save()
    responce = redirect('/login/')
    return responce

