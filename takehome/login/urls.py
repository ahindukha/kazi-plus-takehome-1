from django.urls import path
from .import views
from django.conf.urls.static import static

urlpatterns = [
    path('login/',views.home, name = 'home'),
    path('',views.addinfo, name = 'showform')
]

